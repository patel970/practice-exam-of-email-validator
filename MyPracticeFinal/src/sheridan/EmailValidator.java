package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Dhruvil Patel
 *
 */

public class EmailValidator {
	
	
public static boolean isValidEmail (String email) {
    	
		String myemail = "^[A-Z0-9._%+-]+@[A-Z.-]+\\.[a-zA-Z]{2,7}$";
		Pattern e = Pattern.compile(myemail, Pattern.CASE_INSENSITIVE);
		Matcher m = e.matcher(email);
		return m.find();
		
    	
	}
	
	

}
