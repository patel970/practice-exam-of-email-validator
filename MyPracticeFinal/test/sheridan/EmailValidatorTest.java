package sheridan;

import static org.junit.Assert.*;
import org.junit.Test;
/**
 * 
 * @author Dhruvil Patel
 *
 */

public class EmailValidatorTest {
	
	
	// checking email format <account>@<domain>.<extension>
	@Test
	public void testIsValidEmailRegular() {
		boolean isValidEmail = EmailValidator.isValidEmail("xyz@yahoo.com");
	//	assertTrue( "Invalid email format", isValidEmail);
	}
	@Test
	public void testIsValidEmailException() {
		boolean isValidEmail = EmailValidator.isValidEmail("yourname123@gmail.com");
		//assertTrue( "Invalid email format", isValidEmail);
		}
	@Test
	public void testIsValidEmailBoundaryIn() {
		boolean isValidEmail = EmailValidator.isValidEmail("yourname123@gmail.com");
	//	assertTrue( "Invalid email format", isValidEmail);
		}
	
	@Test
	public void testIsValidEmailBoundaryOut() {
		boolean isValidEmail = EmailValidator.isValidEmail("myusername556@gmail.com");
	//	assertTrue( "Invalid email format", isValidEmail);
		}
	
}